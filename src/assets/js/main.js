AOS.init({
  duration: 800,
  easing: "slide"
});

//(function($) {
//  "use strict";

function ca_main() {
  $(window).stellar({
    responsive: true,
    parallaxBackgrounds: true,
    parallaxElements: true,
    horizontalScrolling: false,
    hideDistantElements: false,
    scrollProperty: "scroll"
  });

  var fullHeight = function() {
    $(".js-fullheight").css("height", $(window).height());
    $(window).resize(function() {
      $(".js-fullheight").css("height", $(window).height());
    });
  };
  fullHeight();

  // loader
  var loader = function() {
    setTimeout(function() {
      if ($("#ftco-loader").length > 0) {
        $("#ftco-loader").removeClass("show");
      }
    }, 1000);
  };
  loader();

  // Scrollax
  $.Scrollax();

  var carousel = function() {
    console.log("inside th vertical");
    setTimeout(function() {
      $(".slick-carousel")
        .slick({
          autoplay: true,
          infinite: true,
          vertical: true,
          verticalSwiping: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          prevArrow: false, //$(".top-arrow"),
          nextArrow: false //$(".bottom-arrow")
        })
        .css({ visibility: "visible" });

      $(".people-carousel")
        .slick({
          autoplay: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          prevArrow: false, //$(".top-arrow"),
          nextArrow: false //$(".bottom-arrow")
        })
        .css({ visibility: "visible" });

      $(".slick-carousel").hover(
        function() {
          $(".ca-tooltipx").show();
        },
        function() {
          $(".ca-tooltipx")
            .text("roll over for full quotes")
            .hide();
        }
      );
    }, 4000);
  };
  carousel();

  $("nav .dropdown").hover(
    function() {
      var $this = $(this);
      // 	 timer;
      // clearTimeout(timer);
      $this.addClass("show");
      $this.find("> a").attr("aria-expanded", true);
      // $this.find('.dropdown-menu').addClass('animated-fast fadeInUp show');
      $this.find(".dropdown-menu").addClass("show");
    },
    function() {
      var $this = $(this);
      // timer;
      // timer = setTimeout(function(){
      $this.removeClass("show");
      $this.find("> a").attr("aria-expanded", false);
      // $this.find('.dropdown-menu').removeClass('animated-fast fadeInUp show');
      $this.find(".dropdown-menu").removeClass("show");
      // }, 100);
    }
  );

  $("#dropdown04").on("show.bs.dropdown", function() {
    console.log("show");
  });

  // scroll
  var scrollWindow = function() {
    $(window).scroll(function() {
      var $w = $(this),
        st = $w.scrollTop(),
        navbar = $(".ftco_navbar"),
        sd = $(".js-scroll-wrap");

      if (st > 150) {
        if (!navbar.hasClass("scrolled")) {
          navbar.addClass("scrolled");
        }
      }
      if (st < 150) {
        if (navbar.hasClass("scrolled")) {
          navbar.removeClass("scrolled sleep");
        }
      }
      if (st > 350) {
        if (!navbar.hasClass("awake")) {
          navbar.addClass("awake");
        }

        if (sd.length > 0) {
          sd.addClass("sleep");
        }
      }
      if (st < 350) {
        if (navbar.hasClass("awake")) {
          navbar.removeClass("awake");
          navbar.addClass("sleep");
        }
        if (sd.length > 0) {
          sd.removeClass("sleep");
        }
      }
    });
  };
  scrollWindow();

  var counter = function() {
    $("#section-counter").waypoint(
      function(direction) {
        if (
          direction === "down" &&
          !$(this.element).hasClass("ftco-animated")
        ) {
          var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(
            ","
          );
          $(".number").each(function() {
            var $this = $(this),
              num = $this.data("number");
            console.log(num);
            $this.animateNumber(
              {
                number: num,
                numberStep: comma_separator_number_step
              },
              7000
            );
          });
        }
      },
      { offset: "95%" }
    );
  };
  counter();

  var contentWayPoint = function() {
    setTimeout(function() {
      var i = 0;
      $(".ftco-animate").waypoint(
        function(direction) {
          if (
            direction === "down" &&
            !$(this.element).hasClass("ftco-animated")
          ) {
            i++;

            $(this.element).addClass("item-animate");
            setTimeout(function() {
              $("body .ftco-animate.item-animate").each(function(k) {
                var el = $(this);

                setTimeout(
                  function() {
                    var effect = el.data("animate-effect");

                    if (effect === "fadeIn") {
                      el.addClass("fadeIn ftco-animated");
                    } else if (effect === "fadeInLeft") {
                      el.addClass("fadeInLeft ftco-animated");
                    } else if (effect === "fadeInRight") {
                      el.addClass("fadeInRight ftco-animated");
                    } else {
                      el.addClass("fadeInUp ftco-animated");
                    }
                    el.removeClass("item-animate");
                  },
                  k * 50,
                  "easeInOutExpo"
                );
              });
            }, 100);
          }
        },
        { offset: "95%" }
      );
    }, 3000);
  };
  contentWayPoint();

  //ca-tooltip

  $(document).on(
    {
      /*
      mouseout: function() {
        //stuff to do on mouse leave
        console.log("mouseleave");
        $(".ca-tooltipx").css({
          visibility: "hidden"
        });
      },
      */

      mouseover: function() {
        //stuff to do on mouse enter
        console.log("x $(window).width()", $(window).width());
        if ($(this).hasClass("ca-quotes") === true) {
          if ($(window).width() < 768) {
            $(".ca-tooltipx, .ca-tooltiptext")
              .finish()
              .hide()
              .css({
                left: event.pageX - 200,
                top: event.pageY - 100
              })
              .show(200)
              .delay(6000)
              .hide(200);
          } else {
          }
        }
      },
      mouseenter: function() {
        //stuff to do on mouse enter
        console.log("mouseenter");
        console.log("this", $(this).hasClass("ca-quotes")); //.find('p').text());

        if ($(this).hasClass("ca-quotes") === true) {
          $(".ca-tooltipx")
            .text(
              $(this)
                .find("p")
                .text()
            )
            .css({
              visibility: "visible"
            });
        }
      },
      touchstart: function(e) {
        //stuff to do on mouse enter
        console.log("touchstart");
        e.stopPropagation();
        //$(".ca-tooltipx").text($(this).find('p').text()).css({
        //  visibility: "visible"
        //});
      }
    },
    ".ca-quotes"
  );

  $(document).on("mousemove", function(event) {
    if ($(window).width() > 768) {
      $(".ca-tooltipx, .ca-tooltiptext").css({
        left: event.pageX + 10,
        top: event.pageY + 10
      });
    }
  });

  // magnific popup
  $(".image-popup").magnificPopup({
    type: "image",
    closeOnContentClick: true,
    closeBtnInside: false,
    fixedContentPos: true,
    mainClass: "mfp-no-margins mfp-with-zoom", // class to remove default margin from left and right side
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
    },
    image: {
      verticalFit: true
    },
    zoom: {
      enabled: true,
      duration: 300 // don't foget to change the duration also in CSS
    }
  });

  $(".popup-youtube, .popup-vimeo, .popup-gmaps").magnificPopup({
    disableOn: 700,
    type: "iframe",
    mainClass: "mfp-fade",
    removalDelay: 160,
    preloader: false,

    fixedContentPos: false
  });

  var bgVideo = function() {
    $(".player").mb_YTPlayer();
  };
  bgVideo();
}
//})(jQuery);
