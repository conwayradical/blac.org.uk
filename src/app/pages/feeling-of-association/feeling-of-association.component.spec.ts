import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeelingOfAssociationComponent } from './feeling-of-association.component';

describe('FeelingOfAssociationComponent', () => {
  let component: FeelingOfAssociationComponent;
  let fixture: ComponentFixture<FeelingOfAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeelingOfAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeelingOfAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
