import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {SnackbarService} from 'ngx-snackbar';
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { NominationService } from "../../nomination.service";
import { PollService } from "../../poll.service";
declare var $: any;
declare var ca_main: any;
@Component({
  selector: "app-brightlights",
  templateUrl: "./brightlights.component.html",
  styleUrls: ["./brightlights.component.scss"]
})
export class BrightlightsComponent implements OnInit {
  form: FormGroup;
  registerform: FormGroup;
  nominationData: any;

  constructor(
    private fb: FormBuilder,
    private rfb: FormBuilder,
    private af: AngularFireDatabase,
    public nominationService: NominationService,
    public pollService: PollService,
    private snackbarService: SnackbarService
  ) {
    this.createForm();
    this.createRegisterForm();
  }

  ngOnInit() {
    $(document).ready(function() {
      ca_main();
    });
  }

  // setUserNominationDoc

  createForm() {
    this.form = this.fb.group({
      name: ["", Validators.required],
      lastname: ["", Validators.required],
      password: ["", Validators.required],
      email: ["", Validators.required],
      pitch: ["", Validators.required],
      phone: ["", Validators.required],
      location: ["", Validators.required],
      category: ["", Validators.required],
      from: ["", Validators.required],
      nominee: ["", Validators.required]
    });
  }

  createRegisterForm() {
    this.registerform = this.rfb.group({
      username: ["", Validators.required],
      lastname: ["", Validators.required],
      email: ["", Validators.required],
      password: ["", Validators.required]
    });
  }

  signout() {
    this.nominationService.signout();
  }

  onSubmit() {
    const {
      name,
      email,
      pitch,
      phone,
      location,
      category,
      from,
      nominee,
      password
    } = this.form.value;
    const date = Date();

    let formRequest = {
      name,
      email,
      pitch,
      phone,
      location,
      category,
      from,
      nominee,
      date,
      password
    };
    this.af.list("/nominations").push(formRequest);
    this.nominationData = {
      email: formRequest.email,
      client: "info@blac.org.uk",
      web: "black.org.uk",
      name: formRequest.name,
      pitch: formRequest.pitch,
      phone: formRequest.phone,
      location: formRequest.location,
      category: formRequest.category,
      from: formRequest.from,
      nominee: formRequest.nominee,
      password: formRequest.password
    };

    this.nominationService.checkNomination(this.nominationData).then(
      results => {
        console.log("sendNominationEmail results : ", results);
        let nom_action: any = results;
        nom_action.code === "nominate"
          ? (this.nominationService.sendNominationEmail(this.nominationData).then(results => {
            console.log(results[0].message); /*console.log("nominate, send email confirming")*/
            this.nominationMsg(results[0].message);
            this.form.reset();
          }))
          : null;
        nom_action.code === "auth/wrong-password"
          ? (console.log(
              "launch modal get correct password, once its correct then nominate, send emai to confirm"
            ), this.registerform.get("email").setValue(this.nominationData.email), this.openModal("login"))
          : null;
        nom_action.code === "auth/user-not-found"
          ? (console.log(
              "user not found: signout if already logged in, register this one and submit nom straight send email to confirm"
            ), this.registerUser(this.nominationData))
          : null;

        //console.log("message for snack bar", results.text);
      },
      error => {
        console.log("sendNominationEmail error : ", error);
      }
    );

    //this.form.reset();
  }

  onSubmit2() {
    const { email, password } = this.registerform.value;
    this.form.get("email").setValue(email)
    this.form.get("password").setValue(password)
    this.closeModal("login");  
    this.onSubmit();
  }

  registerUser(nominationData) {
    this.nominationService.registerUser(nominationData).then(result => {
      //re-submit the form data
      this.onSubmit();
    });
  }

  openModal(id: string) {
    this.pollService.modalopen(id);
  }

  closeModal(id: string) {
    this.pollService.modalclose(id);
  }


  nominationMsg(msg) {
    const _this = this;
    this.snackbarService.add({
      msg: `<strong>${msg}</strong>`,
      timeout: 5000,
      action: {
        text: '',
        onClick: (snack) => {
          console.log('dismissed: ' + snack.id);
          
          _this.undo();
        },
      },
      onAdd: (snack) => {
        console.log('added: ' + snack.id);
      },
      onRemove: (snack) => {
        console.log('removed: ' + snack.id);
      }
    });
  }

  
  clear() {
    this.snackbarService.clear();
  }
  
  undo() {
    console.log('undo?');
  }


}
