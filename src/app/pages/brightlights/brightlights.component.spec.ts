import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrightlightsComponent } from './brightlights.component';

describe('BrightlightsComponent', () => {
  let component: BrightlightsComponent;
  let fixture: ComponentFixture<BrightlightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrightlightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrightlightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
