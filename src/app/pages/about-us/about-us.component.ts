import { Component, OnInit } from "@angular/core";
import { SlidesOutputData, OwlOptions } from "ngx-owl-carousel-o";

declare var $: any;
declare var ca_main: any;

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  customOptions: OwlOptions = {
    autoplay: true,
    autoplayTimeout: 5000,
    autoHeight: true,
    autoWidth: true,
    loop: true,
    animateIn: "fadeInUp",
    //animateOut: 'fadeOutUp',
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    //navText: ["<", ">"],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: true
  };

  activeSlides: SlidesOutputData;
  slideStoreMsg: any = {};
  // 
  slidesStore: any[] = [
    {
      src:
        "../../../assets/images/slider-banner-gold.jpg",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "The greatness of a community is most accurately measured by the compassionate actions of its members - Corretta Scott King (Wife of Martin Luther King Jr)"
    },
    {
      src:
        "https://www.blac.org.uk/wp-content/uploads/2019/02/slider-banner.jpg",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "The greatness of a man is not how much wealth he aquires, but in his integrity and his ability to affect those around him positively - Bob Marley"
    },
    {
      src:
        "https://www.blac.org.uk/wp-content/uploads/2019/02/slider-banner-2.jpg",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "The greatness of a community is most accurately measured by the compassionate actions of its members - Corretta Scott King (Wife of Martin Luther King Jr)"
    },
    {
      src:
        "https://www.blac.org.uk/wp-content/uploads/2019/02/slider-banner-3.jpg",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel - Maya Angelou"
    },
    {
      src:
        "https://www.blac.org.uk/wp-content/uploads/2019/02/slider-banner-4.jpg",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "A people without a knowledge of their history origin and culture is like a tree without roots - Marcus Garvey"
    }
  ];

  constructor() {}

  ngOnInit() {
    $(document).ready(function () {
      ca_main();
     });
  }

  getPassedData(data: SlidesOutputData) {
    this.activeSlides = data;
    if (
      this.slideStoreMsg.message !==
      this.slidesStore[this.activeSlides["startPosition"]].text.split("-")[0]
    ) {
      $(".ca-carousel-fade").hide(500);
    }
    this.slideStoreMsg.message = this.slidesStore[
      this.activeSlides["startPosition"]
    ].text.split("-")[0];
    this.slideStoreMsg.author = this.slidesStore[
      this.activeSlides["startPosition"]
    ].text.split("-")[1];
    $(".ca-carousel-fade").show(1000);

    if ($(window).width() < 767.98) {
      let newbg: any = this.slidesStore[this.activeSlides["startPosition"]].src;
      $(".ca-home").fadeOut("500", function() {
        $(".ca-home").css({
          height: "700px",
          "background-image": "url(" + newbg + ")",
          "background-size": "cover",
          "background-position": "top center"
        });
        $(".ca-home").fadeIn(500);
      });
    }
  }
}

