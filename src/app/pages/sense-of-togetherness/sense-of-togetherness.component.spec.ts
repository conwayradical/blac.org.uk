import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenseOfTogethernessComponent } from './sense-of-togetherness.component';

describe('SenseOfTogethernessComponent', () => {
  let component: SenseOfTogethernessComponent;
  let fixture: ComponentFixture<SenseOfTogethernessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenseOfTogethernessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenseOfTogethernessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
