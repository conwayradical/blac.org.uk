import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { Observable } from "rxjs";
import { SlidesOutputData, OwlOptions } from "ngx-owl-carousel-o";
import { SnackbarService } from "ngx-snackbar";
import { PollService } from "../../poll.service";
//import { NavbarComponent } from "../../components/navbar/navbar.component";

declare var $: any;
declare var ca_main: any;

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  customOptions: OwlOptions = {
    autoplay: true,
    autoplayTimeout: 5000,
    autoHeight: true,
    autoWidth: true,
    loop: true,
    animateIn: "pulse",
    //animateOut: 'fadeOutUp',
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    //navText: ["<", ">"],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 1
      },
      940: {
        items: 1
      }
    },
    nav: true
  };

  activeSlides: SlidesOutputData;
  slideStoreMsg: any = {};
  //
  slidesStore: any[] = [
    {
      src: "../../../assets/images/footer_bg1.png",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "The greatness of a community is most accurately measured by the compassionate actions of its members - Corretta Scott King (Wife of Martin Luther King Jr)"
    },
    {
      src: "../../../assets/images/footer_bg1.png",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "The greatness of a man is not how much wealth he aquires, but in his integrity and his ability to affect those around him positively - Bob Marley"
    },
    {
      src: "../../../assets/images/footer_bg1.png",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "The greatness of a community is most accurately measured by the compassionate actions of its members - Corretta Scott King (Wife of Martin Luther King Jr)"
    },
    {
      src: "../../../assets/images/footer_bg1.png",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel - Maya Angelou"
    },
    {
      src: "../../../assets/images/footer_bg1.png",
      alt: "blac.org.uk",
      title: "blac.org.uk",
      text:
        "A people without a knowledge of their history origin and culture is like a tree without roots - Marcus Garvey"
    }
  ];

  newsletterform: FormGroup;
  pollID: string = "-Lncsf8z-4FnAQ5Ez2Uo";
  pollItems: any;
  voted: boolean;
  voteMsgToggle: boolean = false;
  voteDone: boolean = false;
  pollTitle: any = { key: "", value: "" };
  pollQuestions: any = [];
  options: any = [];
  total: number = 0;
  chartToggle: boolean = false;
  chart = {
    title: "Who do you consider to be Your Role Model?",
    type: "PieChart",
    data: [],
    columnNames: ["Browser", "Percentage"],
    options: {
      colors: [
        "#dc3545",
        "#d4af37",
        "#333333",
        "#f3b49f",
        "#3F250B",
        "#654321"
      ],
      is3D: true
    },
    width: 550,
    height: 400
  };

  constructor(
    public af: AngularFireDatabase,
    private nfb: FormBuilder,
    public pollservice: PollService,
    private snackbarService: SnackbarService,
    public pollService: PollService
  ) {
    this.createNewsletterForm();
  }

  ngOnInit() {
    $(document).ready(function() {
      ca_main();
    });
    this.voted = localStorage.getItem(this.pollID) ? true : false;
    this.chartToggle = this.voted;
    this.getPoll(this.pollID);
    this.voteMsgToggle = false;

    setTimeout(() => {
      //write the users stem info here
      this.openModal("newsletter");
      // end
    }, 15000);
  }

  createNewsletterForm() {
    this.newsletterform = this.nfb.group({
      email: ["", Validators.required]
    });
  }

  vote(option) {
    this.total = 0;
    let currentCount =
      parseInt(this.pollQuestions.find(o => o.key === option)["value"]) + 1;
    console.log("currentCount", currentCount);
    this.pollservice.updatePoll(this.pollID, option, currentCount);
    localStorage.setItem(this.pollID, "true");
    //this.setState({ voted: true, showSnackbar: true })
    this.voted = true;
    // turn on chart
    this.chartToggle = true;
    console.log("do the snack bar for this voted", this.voted);
    this.votedMsg();
  }

  getPoll(id: string) {
    // establish that this poll is live on this siteid
    this.pollservice.pollItems().subscribe(result => {
      const polls = result;
      const poll = polls.find(
        (item: any) => item.key === id && item.value === true
      );
      // get the data
      this.pollItems = this.pollservice.pollData(poll).subscribe(result => {
        const pollData = result;

        //extract the title
        this.pollTitle = pollData.find((item: any) => item.key === "title");

        // create an array with just questions
        this.pollQuestions = pollData.filter(
          (item: any) => item.key !== "title"
        );

        // create chart data
        this.chart.data = this.pollQuestions.map(option => {
          this.total = this.total + parseInt(option.value, 10);
          return [option.key, option.value];
        });
      });
    });
  }

  votedMsg() {
    const _this = this;
    this.snackbarService.add({
      msg: "<strong>Thank you for voting</strong>",
      timeout: 5000,
      action: {
        text: "",
        onClick: snack => {
          console.log("dismissed: " + snack.id);

          _this.undo();
        }
      },
      onAdd: snack => {
        console.log("added: " + snack.id);
      },
      onRemove: snack => {
        console.log("removed: " + snack.id);
      }
    });
  }

  clear() {
    this.snackbarService.clear();
  }

  undo() {
    console.log("undo?");
  }

  getPassedData(data: SlidesOutputData) {
    this.activeSlides = data;
    if (
      this.slideStoreMsg.message !==
      this.slidesStore[this.activeSlides["startPosition"]].text.split("-")[0]
    ) {
      $(".ca-carousel-fade").hide(500);
    }
    this.slideStoreMsg.message = this.slidesStore[
      this.activeSlides["startPosition"]
    ].text.split("-")[0];
    this.slideStoreMsg.author = this.slidesStore[
      this.activeSlides["startPosition"]
    ].text.split("-")[1];
    $(".ca-carousel-fade").show(1000);

    if ($(window).width() < 767.98) {
      let newbg: any = this.slidesStore[this.activeSlides["startPosition"]].src;
      $(".ca-home").fadeOut("500", function() {
        $(".ca-home").css({
          height: "700px",
          "background-image": "url(" + newbg + ")",
          "background-size": "cover",
          "background-position": "top center"
        });
        $(".ca-home").fadeIn(500);
      });
    }
  }
  //ca_loader();
  onSubmit() {
    // send email to db
    const { email } = this.newsletterform.value;
    const date = Date();
    let formRequest = { email, date };
    this.af.list("/contactlist").push(formRequest);
    const contactData = {
      email: formRequest.email,
      client: "info@blac.org.uk",
      web: "black.org.uk",
    };

    this.pollservice.sendContactEmail(contactData).then(
      results => {
        console.log("sendContactEmail results : ", results);
        this.newsletterform.reset();
      },
      error => {
        console.log("sendContactEmail error : ", error);
      }
    );

    this.newsletterform.reset();

    this.closeModal("newsletter");
  }

  openModal(id: string) {
    this.pollService.modalopen(id);
  }

  closeModal(id: string) {
    this.pollService.modalclose(id);
  }

  onSubmit2() {
    this.closeModal("newsletter");
  }
}
