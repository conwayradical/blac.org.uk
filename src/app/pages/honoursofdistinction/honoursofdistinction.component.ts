import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from "angularfire2/database";
import { PollService } from "../../poll.service";
import {SnackbarService} from 'ngx-snackbar';
declare var $: any;
declare var ca_main: any;
@Component({
  selector: 'app-honoursofdistinction',
  templateUrl: './honoursofdistinction.component.html',
  styleUrls: ['./honoursofdistinction.component.scss']
})
export class HonoursofdistinctionComponent implements OnInit {
  
  pollID: string = "-LniNeGWv8gcYu42WZr6";
  pollID2: string = "-LniiUiwavlriGDSCVEw";
  pollID3: string = "-LnimH1v4zLUp9uAU779";

  pollItems: any;
  pollItems2: any;
  pollItems3: any;

  voted: boolean;
  voted2: boolean;
  voted3: boolean;

  voteMsgToggle: boolean = false;
  voteDone:boolean = false;

  pollTitle: any = { key: "", value: "" };
  pollTitle2: any = { key: "", value: "" };
  pollTitle3: any = { key: "", value: "" };

  pollQuestions: any = [];
  pollQuestions2: any = [];
  pollQuestions3: any = [];

  options: any = [];
  total: number = 0;

  constructor(
    public db: AngularFireDatabase,
    public pollservice: PollService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    $(document).ready(function () {
      ca_main();
     });
     this.voted = localStorage.getItem(this.pollID) ? true : false;
     this.voted2 = localStorage.getItem(this.pollID2) ? true : false;
     this.voted3 = localStorage.getItem(this.pollID3) ? true : false;
     // this.chartToggle = this.voted;
     this.getPoll(this.pollID);
     this.getPoll2(this.pollID2);
     this.getPoll3(this.pollID3);
     this.voteMsgToggle = false;
  }
// POLL 1 //////////////////////
  vote(option) {
    this.total = 0;
    let currentCount = parseInt(this.pollQuestions.find(o => o.key === option)["value"]) + 1;
    console.log('currentCount', currentCount);
    this.pollservice.updatePoll(this.pollID, option, currentCount);
    localStorage.setItem(this.pollID, "true");
    //this.setState({ voted: true, showSnackbar: true })
    this.voted = true;
    // turn on chart
    //this.chartToggle = true;
    console.log("do the snack bar for this voted", this.voted);
    this.votedMsg()
  }

  getPoll(id: string) {
    // establish that this poll is live on this siteid
    this.pollservice.pollItems().subscribe(result => {
      const polls = result;
      const poll = polls.find(
        (item: any) => item.key === id && item.value === true
      );
      // get the data
      this.pollItems = this.pollservice.pollData(poll).subscribe(result => {
        const pollData = result;

        //extract the title
        this.pollTitle = pollData.find((item: any) => item.key === "title");

        // create an array with just questions
        this.pollQuestions = pollData.filter(
          (item: any) => item.key !== "title"
        );
      });
    });
  }
// POLL 2 //////////////////////
vote2(option) {
  this.total = 0;
  let currentCount = parseInt(this.pollQuestions2.find(o => o.key === option)["value"]) + 1;
  this.pollservice.updatePoll(this.pollID2, option, currentCount);
  localStorage.setItem(this.pollID2, "true");
  this.voted2 = true;
  this.votedMsg()
}

getPoll2(id: string) {
  // establish that this poll is live on this siteid
  this.pollservice.pollItems().subscribe(result => {
    const polls = result;
    const poll = polls.find(
      (item: any) => item.key === id && item.value === true
    );
    // get the data
    this.pollItems2 = this.pollservice.pollData(poll).subscribe(result => {
      const pollData = result;

      //extract the title
      this.pollTitle2 = pollData.find((item: any) => item.key === "title");

      // create an array with just questions
      this.pollQuestions2 = pollData.filter(
        (item: any) => item.key !== "title"
      );
    });
  });
}
// POLL 3 //////////////////////
vote3(option) {
  this.total = 0;
  let currentCount = parseInt(this.pollQuestions3.find(o => o.key === option)["value"]) + 1;
  this.pollservice.updatePoll(this.pollID3, option, currentCount);
  localStorage.setItem(this.pollID3, "true");
  this.voted3 = true;
  this.votedMsg()
}
getPoll3(id: string) {
  // establish that this poll is live on this siteid
  this.pollservice.pollItems().subscribe(result => {
    const polls = result;
    const poll = polls.find(
      (item: any) => item.key === id && item.value === true
    );
    // get the data
    this.pollItems3 = this.pollservice.pollData(poll).subscribe(result => {
      const pollData = result;

      //extract the title
      this.pollTitle3 = pollData.find((item: any) => item.key === "title");

      // create an array with just questions
      this.pollQuestions3 = pollData.filter(
        (item: any) => item.key !== "title"
      );
    });
  });
}
////////////////////////////////
  votedMsg() {
    const _this = this;
    this.snackbarService.add({
      msg: '<strong>Thank you for voting</strong>',
      timeout: 5000,
      action: {
        text: '',
        onClick: (snack) => {
          console.log('dismissed: ' + snack.id);
          
          _this.undo();
        },
      },
      onAdd: (snack) => {
        console.log('added: ' + snack.id);
      },
      onRemove: (snack) => {
        console.log('removed: ' + snack.id);
      }
    });
  }

  
  clear() {
    this.snackbarService.clear();
  }
  
  undo() {
    console.log('undo?');
  }
}
