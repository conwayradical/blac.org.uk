import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HonoursofdistinctionComponent } from './honoursofdistinction.component';

describe('HonoursofdistinctionComponent', () => {
  let component: HonoursofdistinctionComponent;
  let fixture: ComponentFixture<HonoursofdistinctionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HonoursofdistinctionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HonoursofdistinctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
