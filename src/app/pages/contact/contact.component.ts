import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AngularFireDatabase } from "angularfire2/database";
import { PollService } from "../../poll.service";
declare var $: any;
declare var ca_main: any;
@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.scss"]
})
export class ContactComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private af: AngularFireDatabase,
    public pollservice: PollService
  ) {
    this.createForm();
  }

  ngOnInit() {
    $(document).ready(function() {
      ca_main();
    });
  }

  createForm() {
    this.form = this.fb.group({
      name: ["", Validators.required],
      email: ["", Validators.required],
      message: ["", Validators.required]
    });
  }

  onSubmit() {
    const { name, email, message } = this.form.value;
    const date = Date();
    const html = `
      <div>From: ${name}</div>
      <div>Email: <a href="mailto:${email}">${email}</a></div>
      <div>Date: ${date}</div>
      <div>Message: ${message}</div>
    `;
    let formRequest = { name, email, message, date, html };
    this.af.list("/messages").push(formRequest);
    const contactData = {
      email: formRequest.email,
      client: "info@blac.org.uk",
      web: "black.org.uk",
      name: formRequest.name,
      message: formRequest.message,
    };
    
    this.pollservice.sendEmail(contactData).then(
      results => {
        console.log("sendEmail results : ", results);
        this.form.reset();
      },
      error => {
        console.log("sendEmail error : ", error);
      }
    );
    
   this.form.reset();
  }
}
