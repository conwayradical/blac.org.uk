import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NanaMarfoComponent } from './nana-marfo.component';

describe('NanaMarfoComponent', () => {
  let component: NanaMarfoComponent;
  let fixture: ComponentFixture<NanaMarfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NanaMarfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NanaMarfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
