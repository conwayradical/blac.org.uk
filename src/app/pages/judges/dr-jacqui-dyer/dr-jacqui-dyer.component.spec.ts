import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrJacquiDyerComponent } from './dr-jacqui-dyer.component';

describe('DrJacquiDyerComponent', () => {
  let component: DrJacquiDyerComponent;
  let fixture: ComponentFixture<DrJacquiDyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrJacquiDyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrJacquiDyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
