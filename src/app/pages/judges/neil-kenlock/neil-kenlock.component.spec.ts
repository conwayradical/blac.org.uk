import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NeilKenlockComponent } from './neil-kenlock.component';

describe('NeilKenlockComponent', () => {
  let component: NeilKenlockComponent;
  let fixture: ComponentFixture<NeilKenlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NeilKenlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NeilKenlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
