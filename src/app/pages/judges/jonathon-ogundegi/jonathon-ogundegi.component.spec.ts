import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JonathonOgundegiComponent } from './jonathon-ogundegi.component';

describe('JonathonOgundegiComponent', () => {
  let component: JonathonOgundegiComponent;
  let fixture: ComponentFixture<JonathonOgundegiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JonathonOgundegiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JonathonOgundegiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
