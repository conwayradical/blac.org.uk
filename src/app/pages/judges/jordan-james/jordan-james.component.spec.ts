import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JordanJamesComponent } from './jordan-james.component';

describe('JordanJamesComponent', () => {
  let component: JordanJamesComponent;
  let fixture: ComponentFixture<JordanJamesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JordanJamesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JordanJamesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
