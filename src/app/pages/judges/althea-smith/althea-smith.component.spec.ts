import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AltheaSmithComponent } from './althea-smith.component';

describe('AltheaSmithComponent', () => {
  let component: AltheaSmithComponent;
  let fixture: ComponentFixture<AltheaSmithComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AltheaSmithComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AltheaSmithComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
