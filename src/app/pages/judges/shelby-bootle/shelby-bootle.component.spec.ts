import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelbyBootleComponent } from './shelby-bootle.component';

describe('ShelbyBootleComponent', () => {
  let component: ShelbyBootleComponent;
  let fixture: ComponentFixture<ShelbyBootleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelbyBootleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelbyBootleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
