import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HueyVanderpuijeComponent } from './huey-vanderpuije.component';

describe('HueyVanderpuijeComponent', () => {
  let component: HueyVanderpuijeComponent;
  let fixture: ComponentFixture<HueyVanderpuijeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HueyVanderpuijeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HueyVanderpuijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
