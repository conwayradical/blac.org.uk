import { Component, OnInit } from "@angular/core";

declare var $: any;
declare var ca_main: any;

@Component({
  selector: 'app-division-of-work',
  templateUrl: './division-of-work.component.html',
  styleUrls: ['./division-of-work.component.scss']
})
export class DivisionOfWorkComponent implements OnInit {

  constructor() {}

  ngOnInit() {
    $(document).ready(function () {
      ca_main();
     });
  }

}

