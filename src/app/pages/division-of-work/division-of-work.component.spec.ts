import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivisionOfWorkComponent } from './division-of-work.component';

describe('DivisionOfWorkComponent', () => {
  let component: DivisionOfWorkComponent;
  let fixture: ComponentFixture<DivisionOfWorkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivisionOfWorkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivisionOfWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
