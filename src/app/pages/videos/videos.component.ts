import { Component, OnInit } from '@angular/core';
declare var $: any;
declare var ca_main: any;

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function () {
      ca_main();
     });
  }

}
