import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule }    from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AngularFireModule, } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireFunctionsModule } from '@angular/fire/functions';


import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from "./components/components.module";
import { CarouselModule } from 'ngx-owl-carousel-o';
import { OverlayModule } from '@angular/cdk/overlay';
import { GoogleChartsModule } from 'angular-google-charts';
import {SnackbarModule} from 'ngx-snackbar';

//page compoments
import { AppComponent } from './app.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { HomeComponent } from './pages/home/home.component';
import { JudgesComponent } from './pages/judges/judges.component';
import { VideosComponent } from './pages/videos/videos.component';
import { InformationComponent } from './pages/information/information.component';
import { BrightlightsComponent } from './pages/brightlights/brightlights.component';
import { HonoursofdistinctionComponent } from './pages/honoursofdistinction/honoursofdistinction.component';
import { TicketsComponent } from './pages/tickets/tickets.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NeilKenlockComponent } from './pages/judges/neil-kenlock/neil-kenlock.component';
import { AltheaSmithComponent } from './pages/judges/althea-smith/althea-smith.component';
import { JordanJamesComponent } from './pages/judges/jordan-james/jordan-james.component';
import { DrJacquiDyerComponent } from './pages/judges/dr-jacqui-dyer/dr-jacqui-dyer.component';
import { HueyVanderpuijeComponent } from './pages/judges/huey-vanderpuije/huey-vanderpuije.component';
import { NanaMarfoComponent } from './pages/judges/nana-marfo/nana-marfo.component';
import { ShelbyBootleComponent } from './pages/judges/shelby-bootle/shelby-bootle.component';
import { JonathonOgundegiComponent } from './pages/judges/jonathon-ogundegi/jonathon-ogundegi.component';
import { environment } from 'src/environments/environment';
import { DivisionOfWorkComponent } from './pages/division-of-work/division-of-work.component';
import { SenseOfTogethernessComponent } from './pages/sense-of-togetherness/sense-of-togetherness.component';
import { FeelingOfAssociationComponent } from './pages/feeling-of-association/feeling-of-association.component';



@NgModule({
  declarations: [
    AppComponent,
    AboutUsComponent,
    HomeComponent,
    JudgesComponent,
    VideosComponent,
    InformationComponent,
    BrightlightsComponent,
    HonoursofdistinctionComponent,
    TicketsComponent,
    ContactComponent,
    NeilKenlockComponent,
    AltheaSmithComponent,
    JordanJamesComponent,
    DrJacquiDyerComponent,
    HueyVanderpuijeComponent,
    NanaMarfoComponent,
    ShelbyBootleComponent,
    JonathonOgundegiComponent,
    DivisionOfWorkComponent,
    SenseOfTogethernessComponent,
    FeelingOfAssociationComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireFunctionsModule,
    BrowserAnimationsModule,
    ComponentsModule,
    CarouselModule,
    OverlayModule,
    GoogleChartsModule.forRoot(),
    SnackbarModule.forRoot(),
  ],
  providers: [AngularFirestore, AngularFireDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
