import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
// import * as firebase from 'firebase/app';
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection
} from "angularfire2/firestore";
import { AngularFireFunctions } from "@angular/fire/functions";
import { map, first } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";

const BASE_API_URL = "https://us-central1-peachpub001.cloudfunctions.net";

@Injectable({
  providedIn: "root"
})
export class NominationService {
  siteid: string = "k0rKhlt7b4X6jgsyrThsE4p7vrz2";
  client: string = "Blac Org UK";
  user: any;
  userDoc: any;
  nominationData: any;
  items = [];
  list: Observable<any[]>;
  onUserChanged: BehaviorSubject<any> = new BehaviorSubject({});
  userRef: AngularFirestoreCollection<unknown>;
  docRef: Observable<any[]>;

  onNomineesChanged: BehaviorSubject<any> = new BehaviorSubject({});

  userNomination_collection: AngularFirestoreCollection<
    any
  > = this._fs.collection("users");
  userNomination = this.userNomination_collection.valueChanges();

  constructor(
    private afAuth: AngularFireAuth,
    public _fs: AngularFirestore,
    public _db: AngularFireDatabase,
    public http: HttpClient,
    private fns: AngularFireFunctions
  ) {}

  private async userNominationCats() {
    const snapshot = await this._fs
      .collection(`users/${this.siteid}/nominations/${this.user.uid}/nominees`)
      .ref.get();
    return snapshot.docs.map(doc => doc.data());
  }

  public async isLoggedIn(nominationData) {
    this.nominationData = nominationData;
    const user = await this.afAuth.authState.pipe(first()).toPromise();
    // make this user univeral
    this.user = user;

    return this.userNominationCats().then(result => {
      let gotcategory = [];
      gotcategory = result.filter(
        item => item.category === nominationData.category
      );

      if (this.user && gotcategory.length === 0) {
        setTimeout(() => {
          //write the users stem info here
          this._fs
            .collection(`users/${this.siteid}/nominations`)
            .doc(this.user.uid)
            .set({
              uid: this.user.uid,
              email: this.user.email,
              name: nominationData.name,
              lastname: nominationData.lastname,
              phone: nominationData.phone,
              location: nominationData.location,
              siteid: this.siteid,
              client: this.client
            });
          // end
        }, 1000);

        return this._fs
          .collection(
            `users/${this.siteid}/nominations/${this.user.uid}/nominees`
          )
          .add({
            uid: this.user.uid,
            email: this.user.email,
            name: nominationData.name,
            phone: nominationData.phone,
            location: nominationData.location,
            category: nominationData.category,
            pitch: nominationData.pitch,
            nominee: nominationData.nominee,
            from: nominationData.from,
            siteid: this.siteid,
            client: this.client
          })
          .then(res_1 => {
            return {
              code: res_1.id,
              message: `Thank you for making this nomination, you will be contacted via email to confirm.`
            };
          });
      } else {
        let msg =
          gotcategory.length !== 0
            ? `You have already nominated in this category: ${nominationData.category}, try another category`
            : "You are not logged in";
        return {
          code: "error",
          message: msg
        };
      }
    });
  }

  sendNominationEmail(
    nominationData: any
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([this.isLoggedIn(nominationData)])
        .then(response => {
          console.log("response", response);
          resolve(response);
        })
        .catch(error => {
          console.log(`Error in executing ${error}`);
          reject(error);
        });
    });
  }

  public signout() {
    this.afAuth.auth.signOut();
  }

  public async checkNomination(nominationData: any) {
    // login if the password is wrong modal dah ting with *forgot password* message and link
    try {
      const credentials = await this.afAuth.auth.signInWithEmailAndPassword(
        nominationData.email,
        nominationData.password
      );
      // logged in
      return {
        code: "nominate",
        message: "nominate"
      };
    } catch (err) {
      console.log("Something went wrong:", err.message);
      return err;
    }
  }

  public async registerUser(nominationData) {
    try {
      const credential = await this.afAuth.auth.createUserWithEmailAndPassword(
        nominationData.email,
        nominationData.password
      );
      //start the process
      return {
        code: "registered",
        message: "New user registered Nomination being processed"
      };
    } catch (err) {
      console.log("error: " + err);
    }
  }
}
