import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from '../app-routing.module';

//components
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ModalComponent } from './modal/modal.component';

@NgModule({
  declarations: [NavbarComponent, FooterComponent, ModalComponent],
  imports: [
    AppRoutingModule, BrowserModule, CommonModule
  ],
  exports: [
    NavbarComponent,
    FooterComponent,
    ModalComponent,
  ]
})
export class ComponentsModule { }
