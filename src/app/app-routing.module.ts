import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./pages/home/home.component";
import { AboutUsComponent } from "./pages/about-us/about-us.component";
import { JudgesComponent } from "./pages/judges/judges.component";
import { VideosComponent } from "./pages/videos/videos.component";
import { InformationComponent } from "./pages/information/information.component";
import { BrightlightsComponent } from "./pages/brightlights/brightlights.component";
import { HonoursofdistinctionComponent } from "./pages/honoursofdistinction/honoursofdistinction.component";
import { TicketsComponent } from "./pages/tickets/tickets.component";
import { ContactComponent } from "./pages/contact/contact.component";
import { DivisionOfWorkComponent } from "./pages/division-of-work/division-of-work.component";
import { SenseOfTogethernessComponent } from "./pages/sense-of-togetherness/sense-of-togetherness.component";
import { FeelingOfAssociationComponent } from "./pages/feeling-of-association/feeling-of-association.component";

/* Judgdes */
import { NeilKenlockComponent } from "./pages/judges/neil-kenlock/neil-kenlock.component";
import { AltheaSmithComponent } from "./pages/judges/althea-smith/althea-smith.component";
import { JordanJamesComponent } from "./pages/judges/jordan-james/jordan-james.component";
import { DrJacquiDyerComponent } from "./pages/judges/dr-jacqui-dyer/dr-jacqui-dyer.component";
import { HueyVanderpuijeComponent } from "./pages/judges/huey-vanderpuije/huey-vanderpuije.component";
import { NanaMarfoComponent } from "./pages/judges/nana-marfo/nana-marfo.component";
import { ShelbyBootleComponent } from "./pages/judges/shelby-bootle/shelby-bootle.component";
import { JonathonOgundegiComponent } from "./pages/judges/jonathon-ogundegi/jonathon-ogundegi.component";


const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    component: HomeComponent
  },
  {
    path: "about-us",
    component: AboutUsComponent
  },
  {
    path: "judges",
    component: JudgesComponent
  },
  {
    path: "neil-kenlock",
    component: NeilKenlockComponent
  },
  {
    path: "althea-smith",
    component: AltheaSmithComponent
  },
  {
    path: "jordan-james",
    component: JordanJamesComponent
  },
  {
    path: "jaqui-dyer",
    component: DrJacquiDyerComponent
  },
  {
    path: "huey",
    component: HueyVanderpuijeComponent
  },
  {
    path: "nana-marfo",
    component: NanaMarfoComponent
  },
  {
    path: "shelby-bootle",
    component: ShelbyBootleComponent
  },
  {
    path: "jonathan-ogundegi",
    component: JonathonOgundegiComponent
  },
  {
    path: "videos",
    component: VideosComponent
  },
  {
    path: "information",
    component: InformationComponent
  },
  {
    path: "nominations",
    component: BrightlightsComponent
  },
  {
    path: "honoursofdistinction",
    component: HonoursofdistinctionComponent
  },
  {
    path: "tickets",
    component: TicketsComponent
  },
  {
    path: "contact",
    component: ContactComponent
  },
  {
    path: "division-of-work",
    component: DivisionOfWorkComponent
  },
  {
    path: "sense-of-togetherness",
    component: SenseOfTogethernessComponent
  },
  {
    path: "feeling-of-association",
    component: FeelingOfAssociationComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
