import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
// import * as firebase from 'firebase/app';
import { AngularFireAuth } from "angularfire2/auth";
import { AngularFireDatabase } from "angularfire2/database";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "angularfire2/firestore";
import { AngularFireFunctions } from "@angular/fire/functions";
import { map, first } from "rxjs/operators";

const BASE_API_URL = "https://us-central1-peachpub001.cloudfunctions.net";

@Injectable({
  providedIn: "root"
})
export class PollService {
  siteid: string = "k0rKhlt7b4X6jgsyrThsE4p7vrz2";
  client: string = "Blac Org UK";
  list: Observable<any[]>;
  pollDataList: Observable<any[]>;
  pollDataUpdate: Observable<any[]>;
  messages: Observable<any[]>;

  private modals: any[] = [];

  constructor(
    private afAuth: AngularFireAuth,
    public _fs: AngularFirestore,
    public _db: AngularFireDatabase,
    public http: HttpClient,
    private fns: AngularFireFunctions
  ) {}

  private isLoggedIn() {
    console.log("considering wether use is logged in...");
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  private nominated(user, nominationData) {
    return false;
  }

  public pollItems() {
    this.list = this._db
      .list(`/user-polls/${this.siteid}`)
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map((d: any) => {
            return { key: d.payload.key, value: d.payload.node_.value_ };
          })
        )
      );
    return this.list;
  }
  public pollData(poll) {
    this.pollDataList = this._db
      .list(`/polls/${poll.key}`)
      .snapshotChanges()
      .pipe(
        map(actions =>
          actions.map((d: any) => {
            return { key: d.payload.key, value: d.payload.node_.value_ };
          })
        )
      );
    return this.pollDataList;
  }
  public updatePoll(pollId: any, option: any, newcount: any) {
    console.log("newcount", newcount);
    const itemsRef = this._db.list(`/polls`);
    return itemsRef
      .update(pollId, { [option]: newcount })
      .then(_ => console.log("updated"))
      .catch(err => console.log(err, "You do not have access!"));
  }
  public async sendNominationEmail(nominationData: any) {
    let msg: any = {};
    console.log("sendNominationEmail", nominationData);
    const user = await this.isLoggedIn();
    if (user) {
      if (user.email === nominationData.email) {
        // check category hasn't already been nominated
        console.log('check category hasnt already been nominated')
        if (!this.nominated(user, nominationData)) {
          /**
           * proceed to nomination
           */
          console.log('user has')
          //////////////////////////////////////////////////////////////////////////////
          // this is here if I really do need to make a callable to firebase functions
          // otheriwse it does nothing here. just for show for later if needed
          /*
          this.fns
            .httpsCallable("testFunction")({ message: "Hi there" })
            .toPromise()
            .then(response => {
              console.log(response);
              // return response;
            })
            .catch(err => console.error("error", err));
            */
          //////////////////////////////////////////////////////////////////////////////
          this.setUserNominationDoc(user, nominationData).then(res => {
            console.log('after user has and dnom doc saving')
            return {
              text:
                "Thank you for making this nomination, you will be contacted via email to confirm this",
              email: user.email
            };
          });
        } else {
          /**
           * already nominated, send message
           */
          return { text: "already nominated in this category", email: null };
        }
      } else {
        /**
         * login is not nominator from form
         *
         * just logout as old user and login as new, ask for password in pop
         */
        this.afAuth.auth.signOut();
        return {
          text: "not nominator from form , please login first",
          email: nominationData.email
        };
      }
    } else {
      /**
       * password is present, try to login with it
       * async it please
       */
      console.log('nominationData', nominationData);
      if (nominationData.password) {

        this.afAuth
        .auth
        .signInWithEmailAndPassword(nominationData.email, nominationData.password)
        .then(value => {
          console.log('logged in', value);
          // now send the nominating data
          
          this.setUserNominationDoc(user, nominationData).then(res => {
            return {
              text:
                "Thank you for making this nomination, you will be contacted via email to confirm this",
              email: user.email
            };
          });
          
        })
        .catch(err => {
          console.log('Something went wrong:',err.message);
        });


      } else {
        /**
         * not logged in
         * give choice to login / register
         */
        return {
          text: "not logged in, user needs to login to register nominee",
          email: nominationData.email
        };
      }
    }
  }

  public registerUser(registerData, nominationData) {
    return this.afAuth.auth
      .createUserWithEmailAndPassword(registerData.email, registerData.password)
      .then(credential => {
        return this.setUserNominationDoc(credential.user, registerData);
      })
      .catch(err => {
        // return this.setUserNominationDoc(credential.user, nominationData);
        console.log("error: " + err);

        //const callable = this.fns.httpsCallable('getUserByEmail');
        //return callable({ email: nominationData.email });

        // logout first
        this.afAuth.auth.signOut();
        this.fns
          .httpsCallable("testFunction")({ message: "Hi there" })
          .toPromise()
          .then(response => console.log(response))
          .catch(err => console.error("error", err));

        // Show the error message
      });
  }
  private setUserNominationDoc(user: any, newUserData: any) {
    const userRef: AngularFirestoreDocument<any> = this._fs.doc(
      `users/${this.siteid}/nominations/${user.uid}`
    );
    // newUserData
    const data: any = {
      uid: user.uid,
      email: user.email,
      name: newUserData.name,
      phone: newUserData.phone,
      location: newUserData.location,
      category: newUserData.category,
      pitch: newUserData.pitch,
      nominee: newUserData.nominee,
      from: newUserData.from,
      siteid: this.siteid,
      client: this.client
    };
    console.log("nom data", data);
    return userRef.set(data);
  }
  public sendEmail(contactData: any) {
    //this.messages = _fs.
    return this.afAuth.auth
      .createUserWithEmailAndPassword(contactData.email, "!814c0rg@")
      .then(credential => {
        return this.setUserMessageDoc(credential.user, contactData);
      })
      .catch(err => {
        console.log("error: " + err);
        // Show the error message
        //this.snackBar.open(err, "OK", {
        //  verticalPosition: "top",
        //  duration: 3000
        //});
      });
    /*
    console.log('contactData', contactData); 
    let params = new HttpParams()
      .set("client", contactData.client)
      .set("email", contactData.email)
      .set("web", contactData.web)
      .set("name", contactData.name)
      .set("message", contactData.message);
    
    return this.http.get(BASE_API_URL+'/sendMail', { params: params });
    */
  }
  private setUserMessageDoc(user: any, newUserData: any) {
    const userRef: AngularFirestoreDocument<any> = this._fs.doc(
      `users/${this.siteid}/messages/${user.uid}`
    );
    // newUserData
    const data: any = {
      uid: user.uid,
      email: user.email,
      name: newUserData.name,
      siteid: this.siteid,
      client: this.client,
      message: newUserData.message
    };
    return userRef.set(data);
  }

  private setUserContactDoc(user: any, newUserData: any) {
    const userRef: AngularFirestoreDocument<any> = this._fs.doc(
      `users/${this.siteid}/contactlist/${user.uid}`
    );
    // newUserData
    const data: any = {
      uid: user.uid,
      email: user.email,
      siteid: this.siteid,
      client: this.client,
      password: newUserData.password
    };
    return userRef.set(data);
  }

  public sendContactEmail(contactData: any) {
    
    let password = this.create_password(8);
    contactData.password = password;
    return this.afAuth.auth
      .createUserWithEmailAndPassword(
        contactData.email,
        password
      )
      .then(credential => {
        return this.setUserContactDoc(credential.user, contactData);
      })
      .catch(err => {
        console.log("error: " + err);
      });

  }

  /*
   * Generate comfortable to remember password
   *
   * @param integer length - length of generated password
   *
   * @return string password
   */
  private create_password(length) {
    let vowel = ["a", "e", "i", "o", "u"];

    let consonant = [
      "b",
      "c",
      "d",
      "f",
      "g",
      "h",
      "j",
      "k",
      "l",
      "m",
      "n",
      "p",
      "q",
      "r",
      "s",
      "t",
      "v",
      "w",
      "x",
      "y",
      "z"
    ];

    let result = "";
    for (let i = 0; i < length; i++) {
      if (i % 2 === 0) {
        result += consonant[Math.floor(Math.random() * 15)];
      } else {
        result += vowel[Math.floor(Math.random() * 4)];
      }
    }

    return result;
  }
  /**
   * Modal
   */

  modaladd(modal: any) {
    // add modal to array of active modals
    this.modals.push(modal);
  }

  modalremove(id: string) {
    // remove modal from array of active modals
    this.modals = this.modals.filter(x => x.id !== id);
  }

  modalopen(id: string) {
    console.log('id', id);
    // open modal specified by id
    let modal: any = this.modals.filter(x => x.id === id)[0];
    console.log('modal', modal);
    modal.open();
  }

  modalclose(id: string) {
    // close modal specified by id
    let modal: any = this.modals.filter(x => x.id === id)[0];
    modal.close();
  }
}
